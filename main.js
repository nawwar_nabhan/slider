// defined variable
let items = document.querySelector(".items")
let arrowBack= document.querySelector(".arrowBack")
let arrowForward= document.querySelector(".arrowForward")
let containerWrapper = document.querySelector(".containerWrapper")
let mainContainer = document.querySelector(".mainContainer")

// load items =>the closest way to fetching data from api
document.body.onload = function(){
    for(let i = 0 ; i < 10 ; i++){
        // console.log();
        items.innerHTML += `
                <div class="SingleItem">
                    <div class="imageSection">
                        <img src="https://picsum.photos/id/${235 + i}/300/200" alt="">
                    </div>
                    <div class="title">
                        slider ${i + 1}
                    </div>
                </div>
        `
    }
}


let infinite = 0

// arrow click functionality
containerWrapper.addEventListener("click" , (e)=>{
    items.style.transition = "500ms ease all"
    // console.log(e.target.classList);
    if(e.target.classList.contains("arrowBack")){
            infinite = -1
            items.style.transform = "translateX(300px)"
    }
    if(e.target.classList.contains("arrowForward")){
            infinite = 1
            items.style.transform = "translateX(-300px)"
    }

})

// infinite loop click
items.addEventListener("transitionstart" , ()=>{
    console.log("object");
    
        items.style.transition = 'none';
    if(infinite === 1){
            items.appendChild(items.firstElementChild);
            items.style.transform = "translateX(0)"
        }
        if(infinite === -1){
            items.prepend(items.lastElementChild);
            // items.scrollBy(300 , 0)
            items.style.transform = "translateX(0)"
    }
})


// auto play slider 
let timer  = undefined
let mouseEffectContainer = false 
setInterval(() => {
    if(mouseEffectContainer){
        
            items.style.transform = "translateX(0)"
            items.appendChild(items.firstElementChild);
            items.appendChild(items.firstElementChild);
            items.appendChild(items.firstElementChild);
    }
}, [2000]);
containerWrapper.addEventListener("mouseover" , (e)=>{
    mouseEffectContainer = false
})
containerWrapper.addEventListener("mouseleave" , (e)=>{
    mouseEffectContainer = true
})
containerWrapper.addEventListener("touchstart" , (e)=>{
    mouseEffectContainer = false
})
containerWrapper.addEventListener("touchmove" , (e)=>{
    mouseEffectContainer = false
})
containerWrapper.addEventListener("touchend" , (e)=>{
    mouseEffectContainer = true
})



// mouse move slider 

containerWrapper.addEventListener("mousewheel"  , (e)=>{
    // console.log(e.);
    if(e.deltaX == -1){
        // console.log(e);
            items.prepend(items.lastElementChild);
            items.style.transform = "translateX(0)"
    }
    if(e.deltaX == 1){
        // console.log(e);
            items.appendChild(items.firstElementChild);
            items.style.transform = "translateX(0)"
    }
})




// mainContainer.addEventListener("mouseover" , (e)=>{
//     console.log("touchStart");
//     if(e.target.classList.contains('mainContainer') | e.target.classList.contains("containerWrapper")){
//           timer = setTimeout(()=>{
//             // items.style.transform = "translateX(-300px)"
//             items.style.transition = "500ms ease all"
//             items.appendChild(items.firstElementChild);
//             items.appendChild(items.firstElementChild);
//             items.appendChild(items.firstElementChild);
//         },[500])
//         console.log("object");
//         // console.log(e.target.classList);
//     }
// })
// mainContainer.addEventListener("mouseleave" , (e)=>{
//     clearTimeout(timer)
//     console.log(timer);
// })